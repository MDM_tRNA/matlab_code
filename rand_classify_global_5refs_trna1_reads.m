format short
N = 424
[result, unknowns] = probability(N,2000)
accuracy = sum(result(6,:)==1)/(N-unknowns)


function [b,a]=homo(seq1,seq2,n)
% the third input is the times of the random test we want to run
% the second return value of this function is the optimal global alignment(using Needleman-Wunsch algorithm)score in bits 
% the first return value is the p-value after n times random test
a=nwalign(seq1,seq2,'Alphabet','NT');
c=zeros(1,n);
for i=1:n
    c(1,i)=nwalign(seq1,seq2(randperm(length(seq2))),'Alphabet','NT'); % generate a random sequence of seq2 by shuffling the order of letters in seq2 and align the new Sequence with seq1 
end
max=0;
for i=1:n
    if c(i)>=a
       max=max+1;
    end
end
b=max/n;
end
    

function [result,unknowns] =probability(m,n)
% This function chooses first "m" sequences in the pass file of trna4 to work out p-values by doing "n" random tests. 
B=zeros(m,5);
unknowns = 0;
for i=1:m
    for j=1:5
        x=fastaread('reads_trna1_pass.fa','blockread',i);
        y=fastaread('trna5_ref.fa','blockread',j);
        B(i,j)=homo(x.Sequence,y.Sequence,n);
    end
end
c=zeros(1,m);
for i=1:m
    [score,position]=sort(B(i,:));
    if score(1)~=score(2)
    c(i)=position(1);
    else
        x=fastaread('reads_trna1_pass.fa','blockread',i);
        str1=fastaread('reads_trna1_pass.fa','blockread',position(1));
        str2=fastaread('reads_trna1_pass.fa','blockread',position(2));
        [~,k1]=nwalign(x.Sequence,str1.Sequence,'Alphabet','NT');
        [~,k2]=nwalign(x.Sequence,str2.Sequence,'Alphabet','NT');
        [mat1,sub1,ins1,del1,~]=alignment_analysis(k1);
        [mat2,sub2,ins2,del2,~]=alignment_analysis(k2);
        p1=mat1/(mat1+sub1+ins1+del1);
        p2=mat2/(mat2+sub2+ins2+del2);
        if p1>p2
            c(i)=position(1);
        else
            c(i)=position(2);
        end
    end
    
    if score(1) > 0.025
        c(i) = 0;
        unknowns = unknowns+1;
    end
end
k=B';
result=zeros(6,m);
for i=1:m
    for j=1:5
        result(j,i)=k(j,i);
    end
    result(6,i)=c(i);
end
c
B
end