function b=select_top(ref_file, read_file)
h=fastaread(ref_file,'blockread',1);
x=h.Sequence;
m = length(fastaread(read_file))
k = 420;
a=zeros(1, m);
for i=1:m
    p=fastaread(read_file,'blockread',i);
    [a(1,i),~]=nwalign(x,p.Sequence,'Alphabet','NT'); 
end
[~,location]=sort(a);
b=zeros(k,4);
for i=m-k+1:m
    p=fastaread(read_file,'blockread',location(i));
    [~,w]=nwalign(x,p.Sequence,'Alphabet','NT'); 
    [b(m+1-i,1),b(m+1-i,2),b(m+1-i,4),b(m+1-i,3)]=alignment_analysis(w);
end
l=linspace(1,k,k)';
plot(l,b(:,1),'r-',l,b(:,2),'b-',l,b(:,3),'g-',l,b(:,4),'k-');
title('numbers of four kinds of mutation for top best alignments');
legend('match','substituion','insertion','deletion');
end
    