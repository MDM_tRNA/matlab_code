
data = load('result_class.mat');
reads = fastaread('reads_trna4_pass.fa');
var = fieldnames(data.result_class);

for n = 1:length(var)
    fastawrite(strcat(var{n}, '.fa'), reads(data.result_class.(var{n})));
end
